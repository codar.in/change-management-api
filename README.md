# CM - Api

[![pipeline status](https://gitlab.com/codar.in/change-management-api/badges/master/pipeline.svg)](https://gitlab.com/codar.in/change-management-api/commits/master)
[![coverage report](https://gitlab.com/codar.in/change-management-api/badges/master/coverage.svg)](https://gitlab.com/codar.in/change-management-api/commits/master)

Api to manager your application versions

```bash
export JDBC_URI=jdbc:postgresql://localhost:5432/cm-test?user=sa&password=sa
export JDBC_STRATEGY=create
export CM_BASIC_USERNAME=cm-basic
export CM_BASIC_PASSWORD=abc1234
export APPLICATION_PORT=8090
```

### Docker development mode 

````bash
$ docker run --name=cm -d -p 5433:5433  -e POSTGRES_PASSWORD=sa -e POSTGRES_USER=sa -e POSTGRES_DB=cm postgres
````

### Run Application


```bash
$ java -jar api-0.0.1-SNAPSHOT.jar
```

>Debug mode

```bash
$ java -jar --debug api-0.0.1-SNAPSHOT.jar
```

### Run application with bootstrap mode

```shell
$ java -jar --debug -Dmock=true api-0.0.1-SNAPSHOT.jar
```

### Changelog Pattern

>feat - Feature name

>fix - Fix name


If you can specify ticket name, inform after *fix or feat*.

Ex.: [GM-1501]