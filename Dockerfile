FROM java:8
COPY ./build/libs/api-*.jar /usr/src/cm/api.jar
WORKDIR /usr/src/cm
ENV JDBC_URI="jdbc:postgresql://localhost:5432/cm-test?user=sa&password=sa"
ENV JDBC_STRATEGY="create"
ENV CM_BASIC_USERNAME="cm-basic"
ENV CM_BASIC_PASSWORD="abc1234"
EXPOSE 8080
CMD ["java", "-jar", "api.jar"]