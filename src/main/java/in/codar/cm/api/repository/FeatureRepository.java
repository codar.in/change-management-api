package in.codar.cm.api.repository;

import in.codar.cm.api.domains.Feature;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface FeatureRepository extends JpaRepository<Feature, UUID> {
    List<Feature> findAllByChangelogId(UUID uuid);
}
