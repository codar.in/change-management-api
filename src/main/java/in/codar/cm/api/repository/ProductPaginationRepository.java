package in.codar.cm.api.repository;

import in.codar.cm.api.domains.Product;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.UUID;

public interface ProductPaginationRepository extends PagingAndSortingRepository<Product, UUID> {
}
