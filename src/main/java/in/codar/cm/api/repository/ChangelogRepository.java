package in.codar.cm.api.repository;

import in.codar.cm.api.domains.Changelog;
import in.codar.cm.api.resources.response.ChangelogCount;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface ChangelogRepository extends JpaRepository<Changelog, UUID> {

    List<Changelog> findByProductId(UUID uuid);

    @Query("select createDate, count(c) from Changelog c where createDate > (CURRENT_DATE - ?1) group by createDate")
    public List<Object[]> findAllGroupByCreateDate(Integer days);

}
