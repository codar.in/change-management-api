package in.codar.cm.api.domains;

import com.fasterxml.jackson.annotation.JsonProperty;
import in.codar.cm.api.domains.contracts.DomainConfiguration;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

@MappedSuperclass
public abstract class AbstractDomain implements DomainConfiguration, Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    protected UUID id;

    @JsonProperty("create_date")
    @Temporal(TemporalType.DATE)
    protected Date createDate;

    @JsonProperty("update_date")
    @Temporal(TemporalType.DATE)
    protected Date updateDate;

    @Override
    public UUID getId() {
        return id;
    }

    @Override
    public void setId(UUID id) {
        this.id = id;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    @PreUpdate
    @PrePersist
    public void updateDate() {
        if (this.createDate == null) {
            this.createDate = new Date();
        }
        this.updateDate = new Date();
    }

}
