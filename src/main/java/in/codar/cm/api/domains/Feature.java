package in.codar.cm.api.domains;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.validator.constraints.URL;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Data
@Entity
@EqualsAndHashCode(callSuper = false)
public class Feature extends AbstractDomain {

    private String name;

    private String description;

    @ManyToOne
    @JoinColumn(name = "changelog_id")
    private Changelog changelog;

    @URL
    private String url;

}
