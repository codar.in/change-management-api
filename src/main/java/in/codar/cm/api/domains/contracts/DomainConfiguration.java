package in.codar.cm.api.domains.contracts;

import java.util.UUID;

public interface DomainConfiguration {

    void setId(UUID id);

    UUID getId();
}
