package in.codar.cm.api.domains;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import javax.validation.constraints.Pattern;
import java.util.List;

@Data
@Entity
@EqualsAndHashCode(callSuper = false)
public class Changelog extends AbstractDomain {

    private String branch;

    @JsonProperty("last_commit")
    @Pattern(regexp = "feat-.*|fix-.*|refactor-.*")
    private String lastCommit;

    private String tag;

    private Boolean status;

    @JsonProperty("artifact_url")
    private String artifactUrl;

    private String rfc;

    @ManyToOne
    @JoinColumn(name = "product_id")
    private Product product;

    @OneToMany(orphanRemoval = true, cascade = CascadeType.ALL, mappedBy = "changelog")
    private List<Feature> features;

}
