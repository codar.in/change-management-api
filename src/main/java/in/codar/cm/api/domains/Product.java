package in.codar.cm.api.domains;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.experimental.Tolerate;
import org.hibernate.validator.constraints.URL;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@EqualsAndHashCode(callSuper = false)
public class Product extends AbstractDomain {

    @NonNull
    @Column(nullable = false)
    private String name;

    @NonNull
    @Column(nullable = false)
    private String description;

    @Column(nullable = false, unique = true)
    private String slug;

    @URL
    @NonNull
    @Column(nullable = false)
    private String repository;

    @URL
    @NonNull
    @JsonProperty("workspace_url")
    @Column(nullable = false)
    private String workspaceUrl;

    @OneToMany(orphanRemoval = true, cascade = CascadeType.ALL, mappedBy = "product", fetch = FetchType.EAGER)
    private List<Changelog> changelog;

    @Tolerate
    public Product() {
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", slug='" + slug + '\'' +
                ", repository='" + repository + '\'' +
                ", workspaceUrl='" + workspaceUrl + '\'' +
                ", changelog=" + changelog +
                '}';
    }
}
