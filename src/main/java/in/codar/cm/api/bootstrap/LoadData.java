package in.codar.cm.api.bootstrap;

import in.codar.cm.api.resources.requests.ChangelogRequest;
import in.codar.cm.api.resources.requests.ProductRequest;
import in.codar.cm.api.resources.response.ProductResponse;
import in.codar.cm.api.services.contracts.ChangelogService;
import in.codar.cm.api.services.contracts.ProductService;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class LoadData implements ApplicationListener<ContextRefreshedEvent> {

    private ProductService service;
    private ChangelogService changelogService;

    public LoadData(ProductService service, ChangelogService changelogService) {
        this.service = service;
        this.changelogService = changelogService;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        Boolean mock = Boolean.parseBoolean(System.getProperty("mock", "false"));
        if (mock) {
            this.loadData();
        }
    }

    private void loadData() {

        ProductRequest product = new ProductRequest();

        if (this.service.findBySlug("jira-product") == null) {
            product.setName("Jira Product");
            product.setDescription("Scrum dashboard, and task manager");
            product.setRepository("https://www.github.com.br");
            product.setWorkspace("https://www.jira.com.br");
            ProductResponse productSaved = this.service.save(product);

            ChangelogRequest changelogRequest = new ChangelogRequest();
            changelogRequest.setArtifact("http://www.localhost:8080");
            changelogRequest.setBranch("master");
            changelogRequest.setLastCommit("feat- New feature");
            changelogRequest.setRfc("1222");
            changelogRequest.setTag(String.format("%.2f", (Math.random() * 10)));
            changelogRequest.setStatus(Boolean.TRUE);

            changelogService.createChangelogToProductId(UUID.fromString(productSaved.getId()), changelogRequest);
        }

        if (this.service.findBySlug("github-admin") == null) {
            product = new ProductRequest();
            product.setName("GitHub Admin");
            product.setDescription("SCM - Front end git hub");
            product.setRepository("https://www.github.com.br");
            product.setWorkspace("https://www.jira.com.br");

            ProductResponse productSaved = this.service.save(product);

            ChangelogRequest changelogRequest = new ChangelogRequest();
            changelogRequest.setArtifact("http://www.localhost:8080");
            changelogRequest.setBranch("master");
            changelogRequest.setLastCommit("feat- New feature");
            changelogRequest.setRfc("1222");
            changelogRequest.setTag(String.format("%.2f", (Math.random() * 10)));
            changelogRequest.setStatus(Boolean.TRUE);

            changelogService.createChangelogToProductId(UUID.fromString(productSaved.getId()), changelogRequest);
        }

        if (this.service.findBySlug("salesforce") == null) {
            product = new ProductRequest();
            product.setName("SaleForce");
            product.setDescription("Sales Force");
            product.setRepository("https://www.github.com.br");
            product.setWorkspace("https://www.jira.com.br");

            ProductResponse productSaved = this.service.save(product);

            ChangelogRequest changelogRequest = new ChangelogRequest();
            changelogRequest.setArtifact("http://www.localhost:8080");
            changelogRequest.setBranch("master");
            changelogRequest.setLastCommit("feat- New feature");
            changelogRequest.setRfc("1222");
            changelogRequest.setTag(String.format("%.2f", (Math.random() * 10)));
            changelogRequest.setStatus(Boolean.TRUE);

            changelogService.createChangelogToProductId(UUID.fromString(productSaved.getId()), changelogRequest);
        }
    }
}
