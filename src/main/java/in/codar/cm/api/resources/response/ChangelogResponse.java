package in.codar.cm.api.resources.response;

/**
 * Created by JacksonGenerator on 10/28/18.
 */

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Getter;

import java.util.List;

@Getter
@Builder
public class ChangelogResponse {
    @JsonProperty("artifact")
    private String artifact;
    @JsonProperty("features")
    private List<FeatureResponse> features;
    @JsonProperty("last_commit")
    private String lastCommit;
    @JsonProperty("id")
    private String id;
    @JsonProperty("tag")
    private String tag;
    @JsonProperty("create_date")
    private String createDate;
    @JsonProperty("branch")
    private String branch;
    @JsonProperty("rfc")
    private String rfc;
    @JsonProperty("update_date")
    private String updateDate;
    @JsonProperty("status")
    private Boolean status;
}