package in.codar.cm.api.resources.converts;

import in.codar.cm.api.domains.Changelog;
import in.codar.cm.api.resources.requests.ChangelogRequest;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

@Component
public class ChangelogRequestToChangelogDomain implements Converter<ChangelogRequest, Changelog> {

    @Nullable
    @Override
    public Changelog convert(ChangelogRequest source) {
        Changelog changelog = new Changelog();
        changelog.setArtifactUrl(source.getArtifact());
        changelog.setTag(source.getTag());
        changelog.setBranch(source.getBranch());
        changelog.setRfc(source.getRfc());
        changelog.setLastCommit(source.getLastCommit());
        changelog.setStatus(source.getStatus());
        return changelog;
    }

}
