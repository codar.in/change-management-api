package in.codar.cm.api.resources;

import in.codar.cm.api.resources.requests.ProductRequest;
import in.codar.cm.api.resources.response.ProductResponse;
import in.codar.cm.api.services.contracts.ProductService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.UUID;

import static in.codar.cm.api.resources.ProductResource.BASE_URL;

@RestController
@RequestMapping(value = BASE_URL)
public class ProductResource {

    static final String BASE_URL = "/v1/products";

    private final Logger LOG = LoggerFactory.getLogger(ProductResource.class);

    private ProductService service;

    @Autowired
    public void setService(ProductService service) {
        this.service = service;
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity getById(@PathVariable UUID id) {
        try {

            return ResponseEntity.ok(this.service.findById(id));
        } catch (NoSuchElementException ex) {

            LOG.info("Product not found to id {}", id);
            return ResponseEntity.notFound().build();
        } catch (Exception ex) {

            LOG.error("Error during find product by id {}", id, ex);
            return ResponseEntity.status(500).build();
        }
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity deleteById(@PathVariable UUID id) {
        try {

            this.service.delete(id);
            return ResponseEntity.noContent().build();
        } catch (EmptyResultDataAccessException | NoSuchElementException ex) {

            LOG.info("Product not found to id {}", id);
            return ResponseEntity.notFound().build();
        } catch (Exception ex) {

            LOG.error("Error during find product by id {}", id, ex);
            return ResponseEntity.status(500).build();
        }
    }

    @PutMapping(value = "/{id}", consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity update(@PathVariable UUID id, @RequestBody ProductRequest productRequest) throws URISyntaxException {
        try {
            ProductResponse productResponse = this.service.update(id, productRequest);
            return ResponseEntity
                    .ok(new URI(BASE_URL + productResponse.getId()));

        } catch (NoSuchElementException ex) {

            LOG.info("Product create with success {}", id);
            ProductResponse productResponse = this.service.save(productRequest);
            return ResponseEntity
                    .created(new URI(BASE_URL + productResponse.getId()))
                    .build();
        } catch (DataIntegrityViolationException ex) {

            LOG.error("Product has registered {}", productRequest, ex);
            return ResponseEntity.status(409).build();
        } catch (Exception ex) {

            LOG.error("Error during create product  {}", productRequest, ex);
            return ResponseEntity.status(500).build();
        }
    }

    @PostMapping(consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity create(@RequestBody ProductRequest product) {
        try {

            ProductResponse productResponse = this.service.save(product);
            return ResponseEntity
                    .created(new URI(BASE_URL + productResponse.getId()))
                    .build();
        } catch (DataIntegrityViolationException ex) {

            LOG.error("Product has registered {}", product, ex);
            return ResponseEntity.status(409).build();
        } catch (Exception ex) {

            LOG.error("Error during create product  {}", product, ex);
            return ResponseEntity.status(500).build();
        }
    }

    @GetMapping
    public ResponseEntity findAll() {
        try {
            List<ProductResponse> responses = this.service.findAll();
            LOG.info("Total product found {}", responses.size());
            return ResponseEntity.ok(responses);
        } catch (Exception e) {
            LOG.error("Error during find all product", e);
            return ResponseEntity.status(500).build();
        }
    }

}
