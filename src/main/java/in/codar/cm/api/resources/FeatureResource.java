package in.codar.cm.api.resources;

import in.codar.cm.api.resources.requests.FeatureRequest;
import in.codar.cm.api.resources.response.FeatureResponse;
import in.codar.cm.api.services.contracts.FeatureService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.NoSuchElementException;
import java.util.UUID;

@RestController
@RequestMapping(value = "/v1")
public class FeatureResource {

    private static final Logger LOG = LoggerFactory.getLogger(FeatureResource.class);

    private FeatureService featureService;

    @Autowired
    public FeatureResource(FeatureService featureService) {
        this.featureService = featureService;
    }

    @PostMapping(value = "/changelog/{id}/features")
    public ResponseEntity create(@PathVariable UUID id, @RequestBody FeatureRequest feature) {
        try {

            FeatureResponse result = this.featureService.createFeatureToChangelogId(id, feature);
            LOG.info("Feature registered {}", result);
            return ResponseEntity.created(new URI("/changelog/" + result.getId())).build();
        } catch (NoSuchElementException ex) {

            LOG.error("Feature does not exist {}", id, ex);
            return ResponseEntity.notFound().build();
        } catch (DataIntegrityViolationException ex) {

            LOG.error("Feature has registered {}", id, ex);
            return ResponseEntity.status(409).build();
        } catch (Exception ex) {

            LOG.error("Error during create feature  {}", id, ex);
            return ResponseEntity.status(500).build();
        }
    }
}
