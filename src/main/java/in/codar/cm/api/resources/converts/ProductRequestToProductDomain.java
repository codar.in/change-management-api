package in.codar.cm.api.resources.converts;

import in.codar.cm.api.commons.SlugGenerator;
import in.codar.cm.api.domains.Product;
import in.codar.cm.api.resources.requests.ProductRequest;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

@Component
public class ProductRequestToProductDomain implements Converter<ProductRequest, Product> {

    @Nullable
    @Override
    public Product convert(ProductRequest source) {
        Product product = new Product();
        product.setName(source.getName());
        product.setDescription(source.getDescription());
        product.setRepository(source.getRepository());
        product.setSlug(SlugGenerator.toSlug(source.getName()));
        product.setWorkspaceUrl(source.getWorkspace());
        return product;
    }

}
