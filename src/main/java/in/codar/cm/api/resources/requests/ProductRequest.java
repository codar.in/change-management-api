package in.codar.cm.api.resources.requests;

/**
 * Created by JacksonGenerator on 10/28/18.
 */

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.hibernate.validator.constraints.URL;

import javax.validation.constraints.NotBlank;

@Data
public class ProductRequest {

    @URL
    @JsonProperty("workspace")
    private String workspace;

    @NotBlank
    @JsonProperty("name")
    private String name;

    @NotBlank
    @JsonProperty("description")
    private String description;

    @URL
    @JsonProperty("repository")
    private String repository;
}