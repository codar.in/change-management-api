package in.codar.cm.api.resources.converts;

import in.codar.cm.api.domains.Changelog;
import in.codar.cm.api.domains.Product;
import in.codar.cm.api.resources.response.ChangelogResponse;
import in.codar.cm.api.resources.response.ProductResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ProductDomainToProductResponse implements Converter<Product, ProductResponse> {

    private ChangelogDomainToChangelogResponse domainToChangelogResponse;

    @Autowired
    public ProductDomainToProductResponse(ChangelogDomainToChangelogResponse domainToChangelogResponse) {
        this.domainToChangelogResponse = domainToChangelogResponse;
    }

    @Nullable
    @Override
    public ProductResponse convert(Product source) {
        return ProductResponse
                .builder()
                .name(source.getName())
                .slug(source.getSlug())
                .id(source.getId().toString())
                .createDate(source.getCreateDate().toString())
                .workspace(source.getWorkspaceUrl())
                .repository(source.getRepository())
                .description(source.getDescription())
                .changelog(this.changelogResponseList(source.getChangelog()))
                .build();
    }

    private List<ChangelogResponse> changelogResponseList(List<Changelog> changelogs) {
        List<ChangelogResponse> changelogResponses = new ArrayList<>();
        if (changelogs != null && !changelogs.isEmpty()) {
            changelogs.forEach(changelog -> changelogResponses.add(domainToChangelogResponse.convert(changelog)));
        }
        return changelogResponses;
    }
}
