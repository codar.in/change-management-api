package in.codar.cm.api.resources.converts;

import in.codar.cm.api.domains.Changelog;
import in.codar.cm.api.domains.Feature;
import in.codar.cm.api.resources.response.ChangelogResponse;
import in.codar.cm.api.resources.response.FeatureResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ChangelogDomainToChangelogResponse implements Converter<Changelog, ChangelogResponse> {

    private FeatureDomainToFeatureResponse domainToFeatureResponse;

    @Autowired
    public ChangelogDomainToChangelogResponse(FeatureDomainToFeatureResponse domainToFeatureResponse) {
        this.domainToFeatureResponse = domainToFeatureResponse;
    }

    @Nullable
    @Override
    public ChangelogResponse convert(Changelog source) {
        return ChangelogResponse
                .builder()
                .artifact(source.getArtifactUrl())
                .branch(source.getBranch())
                .createDate(source.getCreateDate().toString())
                .lastCommit(source.getLastCommit())
                .status(source.getStatus())
                .tag(source.getTag())
                .rfc(source.getRfc())
                .updateDate(source.getUpdateDate().toString())
                .id(source.getId().toString())
                .features(this.featureResponses(source.getFeatures()))
                .build();
    }

    private List<FeatureResponse> featureResponses(List<Feature> features) {
        List<FeatureResponse> list = new ArrayList<>();
        if (list != null && !list.isEmpty()) {
            features.forEach(feature -> list.add(domainToFeatureResponse.convert(feature)));
        }
        return list;
    }
}
