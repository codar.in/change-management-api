package in.codar.cm.api.resources.converts;

import in.codar.cm.api.domains.Feature;
import in.codar.cm.api.resources.response.FeatureResponse;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

@Component
public class FeatureDomainToFeatureResponse implements Converter<Feature, FeatureResponse> {

    @Nullable
    @Override
    public FeatureResponse convert(Feature source) {
        return FeatureResponse
                .builder()
                .createDate(source.getCreateDate().toString())
                .updateDate(source.getUpdateDate().toString())
                .description(source.getDescription())
                .name(source.getName())
                .description(source.getDescription())
                .id(source.getId().toString())
                .build();
    }

}
