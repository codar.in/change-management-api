package in.codar.cm.api.resources.response;

/**
 * Created by JacksonGenerator on 10/28/18.
 */

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Getter;

import java.util.List;

@Getter
@Builder
public class ProductResponse {
    @JsonProperty("name")
    private String name;
    @JsonProperty("workspace")
    private String workspace;
    @JsonProperty("description")
    private String description;
    @JsonProperty("changelog")
    private List<ChangelogResponse> changelog;
    @JsonProperty("id")
    private String id;
    @JsonProperty("repository")
    private String repository;
    @JsonProperty("create_date")
    private String createDate;
    @JsonProperty("slug")
    private String slug;
    @JsonProperty("update_date")
    private String updateDate;
}