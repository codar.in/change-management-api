package in.codar.cm.api.resources.response;

/**
 * Created by JacksonGenerator on 10/28/18.
 */

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class FeatureResponse {
    @JsonProperty("name")
    private String name;
    @JsonProperty("description")
    private String description;
    @JsonProperty("id")
    private String id;
    @JsonProperty("create_date")
    private String createDate;
    @JsonProperty("url")
    private String url;
    @JsonProperty("update_date")
    private String updateDate;
}