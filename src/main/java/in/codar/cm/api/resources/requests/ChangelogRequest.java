package in.codar.cm.api.resources.requests;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.hibernate.validator.constraints.URL;

import javax.validation.constraints.Pattern;

@Data
public class ChangelogRequest {
    @URL
    @JsonProperty("artifact")
    private String artifact;

    @Pattern(regexp = "feat-.*|fix-.*|refactor-.*")
    @JsonProperty("last_commit")
    private String lastCommit;

    @JsonProperty("tag")
    private String tag;

    @JsonProperty("branch")
    @Pattern(regexp = "feat-.*|fix-.*|refactor-.*")
    private String branch;

    @JsonProperty("rfc")
    private String rfc;

    @JsonProperty("status")
    private Boolean status;
}