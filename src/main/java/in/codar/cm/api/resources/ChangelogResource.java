package in.codar.cm.api.resources;

import in.codar.cm.api.resources.requests.ChangelogRequest;
import in.codar.cm.api.resources.response.ChangelogCount;
import in.codar.cm.api.resources.response.ChangelogResponse;
import in.codar.cm.api.services.contracts.ChangelogService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.UUID;

import static org.springframework.data.jpa.domain.AbstractPersistable_.id;

@RestController
@RequestMapping("/v1")
public class ChangelogResource {

    private static final Logger LOG = LoggerFactory.getLogger(ChangelogResource.class);

    private ChangelogService changelogService;

    @Autowired
    public ChangelogResource(ChangelogService changelogService) {
        this.changelogService = changelogService;
    }

    @PostMapping(value = "/product/{id}/changelog", consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity create(@PathVariable UUID id, @RequestBody ChangelogRequest changelog) {
        try {

            ChangelogResponse result = this.changelogService.createChangelogToProductId(id, changelog);

            LOG.info("Changelog registered {}", result);
            return ResponseEntity.created(new URI("/changelog/" + result.getId())).build();
        } catch (NoSuchElementException ex) {

            LOG.error("Changelog does not exist {}", id, ex);
            return ResponseEntity.notFound().build();
        } catch (DataIntegrityViolationException ex) {

            LOG.error("Changelog has registered {}", id, ex);
            return ResponseEntity.status(409).build();
        } catch (Exception ex) {

            LOG.error("Error during create product  {}", id, ex);
            return ResponseEntity.status(500).build();
        }
    }

    @GetMapping("/changelog/{id}")
    public ResponseEntity findById(@PathVariable UUID id) {
        try {

            ChangelogResponse changelogResponse = this.changelogService.findById(id);
            LOG.info("Changelog found {}", changelogResponse);
            return ResponseEntity.ok(changelogResponse);
        } catch (NoSuchElementException ex) {

            LOG.error("Changelog does not exist {}", id, ex);
            return ResponseEntity.notFound().build();
        } catch (Exception ex) {

            LOG.error("Error during create Changelog  {}", id, ex);
            return ResponseEntity.status(500).build();
        }
    }

    @GetMapping("/changelog")
    public ResponseEntity findAllChangelogGroupByDate(@RequestParam Integer days) {
        try {

            List<ChangelogCount> changelogCounts = this.changelogService.findAllGroupByCreateDate(days);
            LOG.info("Changelog count {}", changelogCounts.size());
            return ResponseEntity.ok(changelogCounts);
        } catch (Exception ex) {

            LOG.error("Error during create Changelog  {}", id, ex);
            return ResponseEntity.status(500).build();
        }
    }

    @DeleteMapping("/changelog/{id}")
    public ResponseEntity deleteById(@PathVariable UUID id) {
        try {

            this.changelogService.delete(id);
            LOG.info("Changelog found {}", id);
            return ResponseEntity.noContent().build();
        } catch (EmptyResultDataAccessException | NoSuchElementException ex) {

            LOG.error("Changelog does not exist {}", id, ex);
            return ResponseEntity.notFound().build();
        } catch (Exception ex) {

            LOG.error("Error during create product  {}", id, ex);
            return ResponseEntity.status(500).build();
        }
    }

    @PutMapping(value = "/changelog/{id}", consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity updateChangelog(@PathVariable UUID id, @RequestBody ChangelogRequest changelog) {
        try {

            ChangelogResponse result = this.changelogService.update(id, changelog);
            LOG.info("Changelog registered {}", result);
            return ResponseEntity.ok(new URI("/changelog/" + result.getId()));
        } catch (NoSuchElementException ex) {

            LOG.error("Changelog does not exist {}", id, ex);
            return ResponseEntity.notFound().build();
        } catch (Exception ex) {

            LOG.error("Error during create product  {}", id, ex);
            return ResponseEntity.status(500).build();
        }
    }
}
