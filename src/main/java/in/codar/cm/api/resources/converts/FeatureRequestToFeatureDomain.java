package in.codar.cm.api.resources.converts;

import in.codar.cm.api.domains.Feature;
import in.codar.cm.api.resources.requests.FeatureRequest;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

@Component
public class FeatureRequestToFeatureDomain implements Converter<FeatureRequest, Feature> {

    @Nullable
    @Override
    public Feature convert(FeatureRequest source) {
        Feature feature = new Feature();
        feature.setDescription(source.getDescription());
        feature.setName(source.getName());
        feature.setUrl(source.getUrl());
        return feature;
    }

}
