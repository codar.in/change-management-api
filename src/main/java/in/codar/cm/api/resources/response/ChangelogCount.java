package in.codar.cm.api.resources.response;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ChangelogCount {
    private String date;
    private Integer count;
}
