package in.codar.cm.api.services.contracts;

import in.codar.cm.api.resources.requests.ProductRequest;
import in.codar.cm.api.resources.response.ProductResponse;

public interface ProductService extends DomainService<ProductResponse, ProductRequest> {
    ProductResponse findBySlug(String slug);

}
