package in.codar.cm.api.services.implementation;

import in.codar.cm.api.domains.Changelog;
import in.codar.cm.api.domains.Product;
import in.codar.cm.api.repository.ChangelogRepository;
import in.codar.cm.api.repository.ProductRepository;
import in.codar.cm.api.resources.converts.ChangelogDomainToChangelogResponse;
import in.codar.cm.api.resources.converts.ChangelogRequestToChangelogDomain;
import in.codar.cm.api.resources.requests.ChangelogRequest;
import in.codar.cm.api.resources.response.ChangelogCount;
import in.codar.cm.api.resources.response.ChangelogResponse;
import in.codar.cm.api.services.contracts.ChangelogService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class ChangelogServiceImp implements ChangelogService {

    private ProductRepository productRepository;
    private ChangelogRepository changelogRepository;
    private ChangelogDomainToChangelogResponse domainToChangelogResponse;
    private ChangelogRequestToChangelogDomain requestToChangelogDomain;

    public ChangelogServiceImp(ProductRepository productRepository,
                               ChangelogRepository changelogRepository,
                               ChangelogDomainToChangelogResponse domainToChangelogResponse,
                               ChangelogRequestToChangelogDomain requestToChangelogDomain) {
        this.productRepository = productRepository;
        this.changelogRepository = changelogRepository;
        this.domainToChangelogResponse = domainToChangelogResponse;
        this.requestToChangelogDomain = requestToChangelogDomain;
    }

    @Override
    public List<ChangelogResponse> findAllChangelogByProductId(UUID productId) {
        return this.changelogRepository
                .findByProductId(productId)
                .stream().map(p -> this.domainToChangelogResponse.convert(p))
                .collect(Collectors.toList());
    }

    @Override
    public ChangelogResponse createChangelogToProductId(UUID productId, ChangelogRequest changelogRequest) {
        Optional<Product> product = this.productRepository.findById(productId);
        Changelog changelog = this.requestToChangelogDomain.convert(changelogRequest);
        changelog.setProduct(product.get());

        return this.domainToChangelogResponse.convert(this.changelogRepository.save(changelog));
    }

    @Override
    public List<ChangelogCount> findAllGroupByCreateDate(Integer days) {
        List<Object[]> result = this.changelogRepository.findAllGroupByCreateDate(days);
        return result.stream().map(item -> ChangelogCount.builder()
                .date(item[0].toString())
                .count(Integer.valueOf(item[1].toString()))
                .build())
                .collect(Collectors.toList());
    }

    @Override
    public void delete(UUID id) {
        this.changelogRepository.deleteById(id);
    }

    @Override
    public ChangelogResponse findById(UUID id) {
        return this.domainToChangelogResponse.convert(this.changelogRepository.findById(id).get());
    }

    @Override
    public List<ChangelogResponse> findAll() {
        return this.changelogRepository.findAll()
                .stream().map(p -> this.domainToChangelogResponse.convert(p))
                .collect(Collectors.toList());
    }

    @Override
    public ChangelogResponse save(ChangelogRequest object) {
        Changelog changelog = this.requestToChangelogDomain.convert(object);
        return this.domainToChangelogResponse.convert(this.changelogRepository.save(changelog));
    }

    @Override
    public ChangelogResponse update(UUID id, ChangelogRequest object) {
        Optional<Changelog> changelog = changelogRepository.findById(id);
        if (!changelog.isPresent()) {
            throw new NoSuchElementException();
        }
        Changelog convert = this.requestToChangelogDomain.convert(object);
        convert.setId(id);
        convert.setFeatures(changelog.get().getFeatures());
        convert.setProduct(changelog.get().getProduct());
        return this.domainToChangelogResponse.convert(this.changelogRepository.save(convert));
    }

}
