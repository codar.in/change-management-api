package in.codar.cm.api.services.implementation;

import in.codar.cm.api.domains.Product;
import in.codar.cm.api.repository.ProductRepository;
import in.codar.cm.api.resources.converts.ProductDomainToProductResponse;
import in.codar.cm.api.resources.converts.ProductRequestToProductDomain;
import in.codar.cm.api.resources.requests.ProductRequest;
import in.codar.cm.api.resources.response.ProductResponse;
import in.codar.cm.api.services.contracts.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class ProductServiceImp implements ProductService {

    private ProductRepository repository;
    private ProductDomainToProductResponse domainToProductResponse;
    private ProductRequestToProductDomain requestToProductDomain;

    @Autowired
    public ProductServiceImp(ProductRepository repository,
                             ProductDomainToProductResponse productDomainToProductResponse,
                             ProductRequestToProductDomain requestToProductDomain) {
        this.repository = repository;
        this.domainToProductResponse = productDomainToProductResponse;
        this.requestToProductDomain = requestToProductDomain;
    }

    @Override
    public void delete(UUID id) {
        this.repository.deleteById(id);
    }

    @Override
    public ProductResponse findById(UUID id) {
        return domainToProductResponse.convert(this.repository.findById(id).get());

    }

    @Override
    public List<ProductResponse> findAll() {
        return this.repository.findAll()
                .stream()
                .map(product -> domainToProductResponse.convert(product))
                .collect(Collectors.toList());
    }

    @Override
    public ProductResponse save(ProductRequest object) {
        return this.domainToProductResponse.convert(this.repository.save(this.requestToProductDomain.convert(object)));
    }

    @Override
    public ProductResponse update(UUID id, ProductRequest object) {
        Optional<Product> product = repository.findById(id);
        if (!product.isPresent()) {
            throw new NoSuchElementException();
        }
        Product prod = this.requestToProductDomain.convert(object);
        prod.setId(id);
        prod.setChangelog(product.get().getChangelog());
        return this.domainToProductResponse.convert(this.repository.save(prod));
    }

    @Override
    public ProductResponse findBySlug(String slug) {
        Product product = this.repository.findBySlug(slug);
        return product == null ? null : this.domainToProductResponse.convert(product);
    }

}
