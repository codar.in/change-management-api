package in.codar.cm.api.services.contracts;

import java.util.List;
import java.util.UUID;

public interface DomainService<T, S> {

    void delete(UUID id);

    T findById(UUID id);

    List<T> findAll();

    T save(S object);

    T update(UUID id, S object);
}
