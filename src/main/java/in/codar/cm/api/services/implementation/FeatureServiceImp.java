package in.codar.cm.api.services.implementation;

import in.codar.cm.api.domains.Changelog;
import in.codar.cm.api.domains.Feature;
import in.codar.cm.api.repository.ChangelogRepository;
import in.codar.cm.api.repository.FeatureRepository;
import in.codar.cm.api.resources.converts.FeatureDomainToFeatureResponse;
import in.codar.cm.api.resources.converts.FeatureRequestToFeatureDomain;
import in.codar.cm.api.resources.requests.FeatureRequest;
import in.codar.cm.api.resources.response.FeatureResponse;
import in.codar.cm.api.services.contracts.FeatureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class FeatureServiceImp implements FeatureService {

    private FeatureRepository featureRepository;
    private ChangelogRepository changelogRepository;
    private FeatureDomainToFeatureResponse domainToFeatureResponse;
    private FeatureRequestToFeatureDomain featureRequestToFeatureDomain;

    @Autowired
    public FeatureServiceImp(FeatureRepository featureRepository,
                             FeatureDomainToFeatureResponse domainToFeatureResponse,
                             FeatureRequestToFeatureDomain featureRequestToFeatureDomain,
                             ChangelogRepository changelogRepository) {
        this.featureRepository = featureRepository;
        this.domainToFeatureResponse = domainToFeatureResponse;
        this.featureRequestToFeatureDomain = featureRequestToFeatureDomain;
        this.changelogRepository = changelogRepository;
    }

    @Override
    public List<FeatureResponse> findAllByChangelogId(UUID uuid) {
        return this.featureRepository.findAllByChangelogId(uuid)
                .stream().map(p -> this.domainToFeatureResponse.convert(p))
                .collect(Collectors.toList());
    }

    @Override
    public FeatureResponse createFeatureToChangelogId(UUID changelogId, FeatureRequest featureRequest) {
        Optional<Changelog> changelog = this.changelogRepository.findById(changelogId);
        Feature feature = this.featureRequestToFeatureDomain.convert(featureRequest);
        feature.setChangelog(changelog.get());

        return this.domainToFeatureResponse.convert(this.featureRepository.save(feature));
    }

    @Override
    public void delete(UUID id) {
        this.featureRepository.deleteById(id);
    }

    @Override
    public FeatureResponse findById(UUID id) {
        return this.domainToFeatureResponse.convert(this.featureRepository.findById(id).get());
    }

    @Override
    public List<FeatureResponse> findAll() {
        return this.featureRepository
                .findAll().stream()
                .map(p -> this.domainToFeatureResponse.convert(p))
                .collect(Collectors.toList());
    }

    @Override
    public FeatureResponse save(FeatureRequest object) {
        return this.domainToFeatureResponse.convert(
                this.featureRepository.save(
                        this.featureRequestToFeatureDomain.convert(object)
                )
        );
    }

    @Override
    public FeatureResponse update(UUID id, FeatureRequest object) {
        Optional<Feature> product = featureRepository.findById(id);
        if (!product.isPresent()) {
            throw new NoSuchElementException();
        }
        Feature feature = this.featureRequestToFeatureDomain.convert(object);
        feature.setId(id);
        feature.setChangelog(product.get().getChangelog());
        return this.domainToFeatureResponse.convert(this.featureRepository.save(feature));
    }
}
