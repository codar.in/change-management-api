package in.codar.cm.api.services.contracts;

import in.codar.cm.api.resources.requests.ChangelogRequest;
import in.codar.cm.api.resources.response.ChangelogCount;
import in.codar.cm.api.resources.response.ChangelogResponse;

import java.util.List;
import java.util.UUID;

public interface ChangelogService extends DomainService<ChangelogResponse, ChangelogRequest> {

    List<ChangelogResponse> findAllChangelogByProductId(UUID changelogId);

    ChangelogResponse createChangelogToProductId(UUID productId, ChangelogRequest changelogRequest);

    List<ChangelogCount> findAllGroupByCreateDate(Integer days);
}
