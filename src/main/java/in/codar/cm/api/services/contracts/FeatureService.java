package in.codar.cm.api.services.contracts;

import in.codar.cm.api.resources.requests.FeatureRequest;
import in.codar.cm.api.resources.response.FeatureResponse;

import java.util.List;
import java.util.UUID;

public interface FeatureService extends DomainService<FeatureResponse, FeatureRequest> {

    List<FeatureResponse> findAllByChangelogId(UUID changelogId);

    FeatureResponse createFeatureToChangelogId(UUID changelogId, FeatureRequest featureRequest);
}

