package in.codar.cm.api.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.RequestMethod;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.builders.ResponseMessageBuilder;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.ResponseMessage;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Configuration
@EnableSwagger2
public class SwaggerConfiguration {

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("in.codar.cm.api.resources"))
                .paths(PathSelectors.any())
                .build()
                .apiInfo(apiInfo())
                .useDefaultResponseMessages(false)
                .globalResponseMessage(RequestMethod.DELETE,this.commonsResponse())
                .globalResponseMessage(RequestMethod.PATCH,this.commonsResponse())
                .globalResponseMessage(RequestMethod.PUT, this.commonsResponse())
                .globalResponseMessage(RequestMethod.POST, this.commonsResponse())
                .globalResponseMessage(RequestMethod.GET, this.commonsResponse());
    }

    private List<ResponseMessage> commonsResponse() {
        return Arrays.asList(new ResponseMessageBuilder()
                        .code(500)
                        .message("500 message")
                        .responseModel(new ModelRef("ErrorInfo"))
                        .build(),
                new ResponseMessageBuilder()
                        .code(403)
                        .message("Forbidden!")
                        .build(),
                new ResponseMessageBuilder()
                        .code(404)
                        .message("Not found")
                        .build(),
                new ResponseMessageBuilder()
                        .code(201)
                        .message("Created")
                        .build());
    }

    private ApiInfo apiInfo() {
        return new ApiInfo(
                "CM - Change management",
                "System to control change in your apps",
                "1.0.0",
                "https://gitlab.com",
                new Contact("Codar.in", "https://www.codar.in", "codar.in@gmail.com"),
                "Licence to using cm", "https://www.codar.in/api/licence", Collections.emptyList());
    }
}