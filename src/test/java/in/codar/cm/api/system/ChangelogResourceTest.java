package in.codar.cm.api.system;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.javafaker.Faker;
import in.codar.cm.api.resources.requests.ChangelogRequest;
import in.codar.cm.api.resources.requests.ProductRequest;
import in.codar.cm.api.resources.response.ChangelogResponse;
import in.codar.cm.api.resources.response.ProductResponse;
import in.codar.cm.api.services.contracts.ChangelogService;
import in.codar.cm.api.services.contracts.ProductService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.UUID;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class ChangelogResourceTest {

    private static final String BASE_URL_PRODUCT = "/v1/product/";
    private static final String BASE_URL_CHANGELOG = "/v1/changelog/";
    private MockMvc mockMvc;
    private ProductService productService;
    private ChangelogService changelogService;
    private Faker faker;
    private ProductRequest product;
    private ProductResponse productResponse;
    private String json;

    @Value("${cm.basic-authentication.username}")
    private String username;

    @Value("${cm.basic-authentication.password}")
    private String password;

    @Autowired
    public void setProductService(ProductService productService) {
        this.productService = productService;
    }

    @Autowired
    public void setChangelogService(ChangelogService changelogService) {
        this.changelogService = changelogService;
    }

    @Autowired
    public void setMockMvc(MockMvc mockMvc) {
        this.mockMvc = mockMvc;
    }

    @Before
    public void setUp() throws JsonProcessingException {
        this.faker = new Faker();

        this.product = new ProductRequest();
        this.product.setName(this.faker.name().fullName());
        this.product.setDescription(this.faker.lorem().paragraph());
        this.product.setRepository("https://www.github.com.br");
        this.product.setWorkspace("https://www.jira.com.br");

        this.productResponse = this.productService.save(this.product);

        ObjectMapper mapper = new ObjectMapper();

        ChangelogRequest changelog = new ChangelogRequest();
        changelog.setArtifact("https://www.nexus.com.br");
        changelog.setBranch("master");
        changelog.setLastCommit("feat-create first commit");

        this.json = mapper.writeValueAsString(changelog);
    }

    @Test
    public void createChangelog() throws Exception {

        this.mockMvc.perform(post(BASE_URL_PRODUCT + this.productResponse.getId() + "/changelog")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON_UTF8)
                .content(this.json)
                .with(httpBasic(this.username, this.password)))
                .andExpect(status().isCreated());
    }

    @Test
    public void createChangelogWhenProductNotFound() throws Exception {

        this.mockMvc.perform(post(BASE_URL_PRODUCT + "d0e2d318-4cae-4e6f-ad08-43972d50e407" + "/changelog")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON_UTF8)
                .content(this.json)
                .with(httpBasic(this.username, this.password)))
                .andExpect(status().isNotFound());
    }

    @Test
    public void findById() throws Exception {

        ChangelogRequest changelogRequest = new ChangelogRequest();
        changelogRequest.setArtifact("https://www.nexus.com.br");
        changelogRequest.setBranch("master");
        changelogRequest.setLastCommit("feat-create first commit");

        ChangelogResponse response = this.changelogService.createChangelogToProductId(UUID.fromString(productResponse.getId()), changelogRequest);

        this.mockMvc.perform(get(BASE_URL_CHANGELOG + response.getId())
                .with(httpBasic(this.username, this.password)))
                .andExpect(status().isOk());
    }

    @Test
    public void findByIdWheChangelogNotFound() throws Exception {

        this.mockMvc.perform(get(BASE_URL_CHANGELOG + "d0e2d318-4cae-4e6f-ad08-43972d50e407")
                .content(this.json)
                .with(httpBasic(this.username, this.password)))
                .andExpect(status().isNotFound());
    }

    @Test
    public void deleteChangelogFromById() throws Exception {

        ChangelogRequest changelogRequest = new ChangelogRequest();
        changelogRequest.setArtifact("https://www.nexus.com.br");
        changelogRequest.setBranch("master");
        changelogRequest.setLastCommit("feat-create first commit");

        ChangelogResponse response = this.changelogService.createChangelogToProductId(UUID.fromString(productResponse.getId()), changelogRequest);

        this.mockMvc.perform(delete(BASE_URL_CHANGELOG + response.getId())
                .content(this.json)
                .with(httpBasic(this.username, this.password)))
                .andExpect(status().isNoContent());
    }

    @Test
    public void deleteChangelogFromByIdWhenIdNotFound() throws Exception {

        this.mockMvc.perform(delete(BASE_URL_CHANGELOG + "d0e2d318-4cae-4e6f-ad08-43972d50e407")
                .content(this.json)
                .with(httpBasic(this.username, this.password)))
                .andExpect(status().isNotFound());
    }

    @Test
    public void updateChangelog() throws Exception {

        ChangelogRequest changelogRequest = new ChangelogRequest();
        changelogRequest.setArtifact("https://www.nexus.com.br");
        changelogRequest.setBranch("master");
        changelogRequest.setLastCommit("feat-create first commit");

        ChangelogResponse response = this.changelogService.createChangelogToProductId(UUID.fromString(productResponse.getId()), changelogRequest);

        this.mockMvc.perform(put(BASE_URL_CHANGELOG + response.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON_UTF8)
                .content(this.json)
                .with(httpBasic(this.username, this.password)))
                .andExpect(status().isOk());
    }

    @Test
    public void updateChangelogWhenIdNotFound() throws Exception {

        this.mockMvc.perform(put(BASE_URL_CHANGELOG + "d0e2d318-4cae-4e6f-ad08-43972d50e407")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON_UTF8)
                .content(this.json)
                .with(httpBasic(this.username, this.password)))
                .andExpect(status().isNotFound());
    }
}
