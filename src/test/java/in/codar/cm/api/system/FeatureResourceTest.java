package in.codar.cm.api.system;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.javafaker.Faker;
import in.codar.cm.api.resources.requests.ChangelogRequest;
import in.codar.cm.api.resources.requests.FeatureRequest;
import in.codar.cm.api.resources.requests.ProductRequest;
import in.codar.cm.api.resources.response.ChangelogResponse;
import in.codar.cm.api.resources.response.ProductResponse;
import in.codar.cm.api.services.contracts.ChangelogService;
import in.codar.cm.api.services.contracts.FeatureService;
import in.codar.cm.api.services.contracts.ProductService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.UUID;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class FeatureResourceTest {

    private static final String BASE_URL_CHANGELOG = "/v1/changelog/";
    private static final String BASE_URL_FEATURE = "/v1/features/";
    private MockMvc mockMvc;
    private ProductService productService;
    private ChangelogService changelogService;
    private FeatureService featureService;
    @Value("${cm.basic-authentication.username}")
    private String username;
    @Value("${cm.basic-authentication.password}")
    private String password;
    private Faker faker;
    private ChangelogResponse changelog;
    private FeatureRequest feature;
    private String json;

    @Autowired
    public void setMockMvc(MockMvc mockMvc) {
        this.mockMvc = mockMvc;
    }

    @Autowired
    public void setProductService(ProductService productService) {
        this.productService = productService;
    }

    @Autowired
    public void setChangelogService(ChangelogService changelogService) {
        this.changelogService = changelogService;
    }

    @Autowired
    public void setFeatureService(FeatureService featureService) {
        this.featureService = featureService;
    }

    @Before
    public void setUp() throws JsonProcessingException {
        this.faker = new Faker();

        ProductRequest product = new ProductRequest();
        product.setName(faker.name().fullName());
        product.setDescription(faker.lorem().paragraph());
        product.setRepository("https://www.github.com.br");
        product.setWorkspace("https://www.jira.com.br");

        ProductResponse productResponse = this.productService.save(product);

        ChangelogRequest changelogRequest = new ChangelogRequest();
        changelogRequest.setArtifact("https://www.nexus.com.br");
        changelogRequest.setBranch("master");
        changelogRequest.setLastCommit("feat-create first commit");

        this.changelog = this.changelogService.createChangelogToProductId(UUID.fromString(productResponse.getId()), changelogRequest);

        this.feature = new FeatureRequest();

        feature.setName(this.faker.name().fullName());
        feature.setDescription("description feature");

        ObjectMapper mapper = new ObjectMapper();

        this.json = mapper.writeValueAsString(this.feature);
    }

    @Test
    public void testCreateFeature() throws Exception {

        this.mockMvc.perform(post(BASE_URL_CHANGELOG + this.changelog.getId() + "/features")
                .with(httpBasic(this.username, this.password))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON_UTF8)
                .content(this.json))
                .andExpect(status().isCreated());
    }
}