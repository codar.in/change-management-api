package in.codar.cm.api.system;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.javafaker.Faker;
import in.codar.cm.api.resources.requests.ProductRequest;
import in.codar.cm.api.resources.response.ProductResponse;
import in.codar.cm.api.services.contracts.ProductService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class ProductResourceTest {

    private static final String PRODUCT = "/v1/products/";
    @Autowired
    private MockMvc mockMvc;

    private String json;

    @Autowired
    private ProductService productService;

    private ProductRequest product;

    @Value("${cm.basic-authentication.username}")
    private String username;

    @Value("${cm.basic-authentication.password}")
    private String password;

    @Before
    public void setUp() throws Exception {

        Faker faker = new Faker();

        this.product = new ProductRequest();
        product.setName(faker.name().fullName());
        product.setDescription(faker.lorem().paragraph());
        product.setRepository("https://www.github.com.br");
        product.setWorkspace("https://www.jira.com.br");

        ObjectMapper mapper = new ObjectMapper();

        this.json = mapper.writeValueAsString(product);
    }

    @Test
    public void testCreateProduct() throws Exception {

        this.mockMvc.perform(post(PRODUCT)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON_UTF8)
                .content(this.json)
                .with(httpBasic(this.username, this.password)))
                .andExpect(status().isCreated());
    }

    @Test
    public void testCreateProductWithConflict() throws Exception {

        this.mockMvc.perform(post(PRODUCT)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON_UTF8)
                .content(this.json)
                .with(httpBasic(this.username, this.password)))
                .andExpect(status().isCreated());

        this.mockMvc.perform(post(PRODUCT)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON_UTF8)
                .content(this.json)
                .with(httpBasic(this.username, this.password)))
                .andExpect(status().isConflict());
    }

    @Test
    public void testFindProductById() throws Exception {

        ProductResponse product = this.productService.save(this.product);

        this.mockMvc.perform(get(PRODUCT + product.getId())
                .with(httpBasic(this.username, this.password)))
                .andExpect(status().isOk());
    }

    @Test
    public void testRemoveByIdWhenIdNotFound() throws Exception {

        this.mockMvc.perform(delete(PRODUCT + "d0e2d318-4cae-4e6f-ad08-43972d50e407")
                .with(httpBasic(this.username, this.password)))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testRemoveById() throws Exception {

        ProductResponse product = this.productService.save(this.product);

        this.mockMvc.perform(get(PRODUCT + product.getId())
                .with(httpBasic(this.username, this.password)))
                .andExpect(status().isOk());

        this.mockMvc.perform(delete(PRODUCT + product.getId())
                .with(httpBasic(this.username, this.password)))
                .andExpect(status().isNoContent());
    }

    @Test
    public void testUpdateProduct() throws Exception {
        this.product.setName("Product teste 1");
        ProductResponse product = this.productService.save(this.product);

        this.mockMvc.perform(put(PRODUCT + product.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON_UTF8)
                .content(this.json)
                .with(httpBasic(this.username, this.password)))
                .andExpect(status().isOk());
    }

    @Test
    public void testCreateProductFromPutMethod() throws Exception {
        this.mockMvc.perform(put(PRODUCT+ "d0e2d318-4cae-4e6f-ad08-43972d50e407")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON_UTF8)
                .content(this.json)
                .with(httpBasic(this.username, this.password)))
                .andExpect(status().isCreated());
    }
}
