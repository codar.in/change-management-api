package in.codar.cm.api.integration;

import com.github.javafaker.Faker;
import in.codar.cm.api.resources.requests.ProductRequest;
import in.codar.cm.api.resources.response.ProductResponse;
import in.codar.cm.api.services.contracts.ProductService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.UUID;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class ProductServiceTest {

    @Autowired
    private ProductService service;

    private Faker faker;

    @Before
    public void setUp() {
        this.faker = new Faker();
    }

    @Test
    public void testCreateProduct() {
        ProductRequest product = new ProductRequest();
        product.setName(this.faker.name().fullName());
        product.setDescription(this.faker.lorem().paragraph());
        product.setRepository("https://www.github.com.br");
        product.setWorkspace("https://www.jira.com.br");

        ProductResponse response = this.service.save(product);

        ProductResponse productResponse = this.service.findById(UUID.fromString(response.getId()));

        assert response.getName().equals(productResponse.getName());
    }

    @Test(expected = NullPointerException.class)
    public void testCreateProductWithInsufficientField() {

        ProductRequest product = new ProductRequest();
        product.setName(this.faker.name().fullName());
        product.setRepository("https://www.github.com.br");
        product.setWorkspace("https://www.jira.com.br");

        this.service.save(product);
    }

    @Test
    public void testGetAllProducts() {
        ProductRequest product = new ProductRequest();
        product.setName(this.faker.name().fullName());
        product.setDescription(this.faker.lorem().paragraph());
        product.setRepository("https://www.github.com.br");
        product.setWorkspace("https://www.jira.com.br");

        ProductResponse response = this.service.save(product);

        List<ProductResponse> all = this.service.findAll();

        assert all.size() > 0;
    }

    @Test
    public void testGetProductBySlug() {
        ProductRequest product = new ProductRequest();
        product.setName(this.faker.name().fullName());
        product.setDescription(this.faker.lorem().paragraph());
        product.setRepository("https://www.github.com.br");
        product.setWorkspace("https://www.jira.com.br");

        ProductResponse response = this.service.save(product);

        ProductResponse productResponse = this.service.findBySlug(response.getSlug());

        assert productResponse != null;
    }

    @Test(expected = NoSuchElementException.class)
    public void testDeleteProduct() {

        ProductRequest product = new ProductRequest();
        product.setName(this.faker.name().fullName());
        product.setDescription(this.faker.lorem().paragraph());
        product.setRepository("https://www.github.com.br");
        product.setWorkspace("https://www.jira.com.br");

        ProductResponse response = this.service.save(product);

        assert response.getId() != null;

        this.service.delete(UUID.fromString(response.getId()));

        this.service.findById(UUID.fromString(response.getId()));
    }
}
