package in.codar.cm.api.integration;

import com.github.javafaker.Faker;
import in.codar.cm.api.resources.requests.ChangelogRequest;
import in.codar.cm.api.resources.requests.FeatureRequest;
import in.codar.cm.api.resources.requests.ProductRequest;
import in.codar.cm.api.resources.response.ChangelogResponse;
import in.codar.cm.api.resources.response.FeatureResponse;
import in.codar.cm.api.resources.response.ProductResponse;
import in.codar.cm.api.services.contracts.ChangelogService;
import in.codar.cm.api.services.contracts.FeatureService;
import in.codar.cm.api.services.contracts.ProductService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.UUID;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class FeatureServiceTest {

    private ProductRequest product;
    private ChangelogRequest changelog;
    private ChangelogResponse changelogResponse;

    private Faker faker;

    private ChangelogService changelogService;
    private ProductService productService;
    private FeatureService featureService;

    @Before
    public void init() {

        this.faker = new Faker();

        this.product = new ProductRequest();
        this.product.setName(this.faker.name().fullName());
        this.product.setDescription(this.faker.lorem().paragraph());
        this.product.setRepository("https://www.github.com.br");
        this.product.setWorkspace("https://www.jira.com.br");


        ProductResponse productResponse = this.productService.save(this.product);

        this.changelog = new ChangelogRequest();
        changelog.setArtifact("https://www.nexus.com.br");
        changelog.setBranch("master");
        changelog.setLastCommit("feat-create first commit");


        this.changelogResponse = this.changelogService.createChangelogToProductId(UUID.fromString(productResponse.getId()), changelog);
    }

    @Autowired
    public void setChangelogService(ChangelogService changelogService) {
        this.changelogService = changelogService;
    }

    @Autowired
    public void setProductService(ProductService productService) {
        this.productService = productService;
    }

    @Autowired
    public void setFeatureService(FeatureService featureService) {
        this.featureService = featureService;
    }

    @Test
    public void createFeature() {

        FeatureRequest featureRequest = new FeatureRequest();

        featureRequest.setName(this.faker.name().fullName());
        featureRequest.setDescription("description feature");

        FeatureResponse feature = this.featureService.createFeatureToChangelogId(UUID.fromString(changelogResponse.getId()), featureRequest);

        assert feature.getId() != null;
    }

    @Test
    public void getAllFeaturesFromChangelog() {
        FeatureRequest featureRequest = new FeatureRequest();

        featureRequest.setName(this.faker.name().fullName());
        featureRequest.setDescription("description feature");

        this.featureService.createFeatureToChangelogId(UUID.fromString(changelogResponse.getId()), featureRequest);

        featureRequest = new FeatureRequest();

        featureRequest.setName(this.faker.name().fullName());
        featureRequest.setDescription("description feature");

        this.featureService.createFeatureToChangelogId(UUID.fromString(changelogResponse.getId()), featureRequest);

        List<FeatureResponse> allByChangelogId = this.featureService.findAllByChangelogId(UUID.fromString(this.changelogResponse.getId()));

        assert allByChangelogId.size() > 0;
    }

    @Test
    public void findById() {

        FeatureRequest featureRequest = new FeatureRequest();

        featureRequest.setName(this.faker.name().fullName());
        featureRequest.setDescription("description feature");

        FeatureResponse featureToChangelogId = this.featureService.createFeatureToChangelogId(UUID.fromString(changelogResponse.getId()), featureRequest);

        FeatureResponse response = this.featureService.findById(UUID.fromString(featureToChangelogId.getId()));

        assert response != null;

        assert response.getName().equals(featureRequest.getName());

    }

    @Test(expected = NoSuchElementException.class)
    public void removeById() {
        FeatureRequest featureRequest = new FeatureRequest();

        featureRequest.setName(this.faker.name().fullName());
        featureRequest.setDescription("description feature");

        FeatureResponse featureToChangelogId = this.featureService.createFeatureToChangelogId(UUID.fromString(changelogResponse.getId()), featureRequest);

        this.featureService.delete(UUID.fromString(featureToChangelogId.getId()));

        this.featureService.findById(UUID.fromString(featureToChangelogId.getId()));
    }
}
