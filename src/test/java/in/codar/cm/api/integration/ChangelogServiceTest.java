package in.codar.cm.api.integration;

import com.github.javafaker.Faker;
import in.codar.cm.api.resources.requests.ChangelogRequest;
import in.codar.cm.api.resources.requests.ProductRequest;
import in.codar.cm.api.resources.response.ChangelogResponse;
import in.codar.cm.api.resources.response.ProductResponse;
import in.codar.cm.api.services.contracts.ChangelogService;
import in.codar.cm.api.services.contracts.ProductService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;
import java.util.UUID;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class ChangelogServiceTest {

    private ProductRequest product;
    private ProductResponse productResponse;

    private Faker faker;

    private ChangelogService changelogService;
    private ProductService productService;

    @Before
    public void init() {
        this.faker = new Faker();

        this.product = new ProductRequest();
        this.product.setName(this.faker.name().fullName());
        this.product.setDescription(this.faker.lorem().paragraph());
        this.product.setRepository("https://www.github.com.br");
        this.product.setWorkspace("https://www.jira.com.br");


        this.productResponse = this.productService.save(this.product);
    }

    @Autowired
    public void setChangelogService(ChangelogService changelogService) {
        this.changelogService = changelogService;
    }

    @Autowired
    public void setProductService(ProductService productService) {
        this.productService = productService;
    }

    @Test
    public void createChangelog() {
        ChangelogRequest changelogRequest = new ChangelogRequest();
        changelogRequest.setArtifact("https://www.nexus.com.br");
        changelogRequest.setBranch("master");
        changelogRequest.setLastCommit("feat-create first commit");


        ChangelogResponse changelogToProductId = this.changelogService.createChangelogToProductId(UUID.fromString(productResponse.getId()), changelogRequest);

        assert changelogToProductId.getId() != null;
    }

    @Test
    public void getAllChangelogFromProduct() {

        ChangelogRequest changelogRequest = new ChangelogRequest();
        changelogRequest.setArtifact("https://www.nexus.com.br");
        changelogRequest.setBranch("master");
        changelogRequest.setLastCommit("feat-create first commit");

        this.changelogService.createChangelogToProductId(UUID.fromString(productResponse.getId()), changelogRequest);

        changelogRequest = new ChangelogRequest();
        changelogRequest.setArtifact("https://www.nexus.com.br");
        changelogRequest.setBranch("master");
        changelogRequest.setLastCommit("feat-create first commit");

        this.changelogService.createChangelogToProductId(UUID.fromString(productResponse.getId()), changelogRequest);

        List<ChangelogResponse> allChangelogByProductId = this.changelogService.findAllChangelogByProductId(UUID.fromString(productResponse.getId()));

        assert allChangelogByProductId.size() > 0;

    }

    @Test
    public void findById() {
        ChangelogRequest changelogRequest = new ChangelogRequest();
        changelogRequest.setArtifact("https://www.nexus.com.br");
        changelogRequest.setBranch("master");
        changelogRequest.setLastCommit("feat-create first commit");

        ChangelogResponse changelogToProductId = this.changelogService.createChangelogToProductId(UUID.fromString(productResponse.getId()), changelogRequest);

        assert changelogToProductId.getId() != null;

        ChangelogResponse changelogServiceById = this.changelogService.findById(UUID.fromString(changelogToProductId.getId()));

        assert changelogServiceById.getLastCommit().equals(changelogRequest.getLastCommit());
    }
}
